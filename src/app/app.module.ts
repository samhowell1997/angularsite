import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { DataDisplayComponent } from './Components/data-display/data-display.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ToolBarComponent }from './Components/tool-bar/tool-bar.component';
import { MainComponentComponent } from './Components/main-component/main-component.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {DragDropModule} from '@angular/cdk/drag-drop';
import{MatCardModule} from '@angular/material/card';
import { DragableCardComponent } from './dragable-card/dragable-card.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PieChartComponent } from './Components/pie-chart/pie-chart.component';
import { BarChartComponent } from './Components/bar-chart/bar-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    DataDisplayComponent,
    ToolBarComponent,
    MainComponentComponent,
    DragableCardComponent,
    PieChartComponent,
    BarChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    NoopAnimationsModule,
    DragDropModule,
    MatCardModule,
    NgxChartsModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
