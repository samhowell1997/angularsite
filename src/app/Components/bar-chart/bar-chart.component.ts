
import { Component, NgModule, OnInit, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Country } from 'src/app/Models/country';
@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

   // input width of graph
   @Input() Width: number; 
   // output width of graph
   @Input() Height: number; 
   // input raw data for graph
   @Input() GraphData: Country[]; 

  // object store for graph data 
  single: any[];
  // graph dimensions
  view: any[] ;

  // options for graph
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Probability';

 
  // colour scheme for graph
  colorScheme = {
    domain: ['#8babd6', '#96c9d0', '#ffc30a', '#fff151']
  };


  constructor() {
  
  }

  // initialize view and data
  ngOnInit(): void {
    this.view = [this.Width-20, this.Height];
    this.setGraphReadyData();
  }

  // set single object in accordance with required graph data format 
  setGraphReadyData():void{
    var single=[];
    this.GraphData.forEach(element => {
      single.push({name:element.country_id, value:element.probability});
    });
    Object.assign(this, { single });
  }
  
  // debuging methods
  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
