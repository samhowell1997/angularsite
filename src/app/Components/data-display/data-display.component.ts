import { Component, Input, OnInit } from '@angular/core';

import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-data-display',
  templateUrl: './data-display.component.html',
  styleUrls: ['./data-display.component.css']
})
export class DataDisplayComponent implements OnInit {

  constructor() { }
  // input for names to dislpay
  @Input()NamesToDisplay = [];
 
  ngOnInit(): void {
  
  }

}
