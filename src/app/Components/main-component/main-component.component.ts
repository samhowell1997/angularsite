import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.css']
})
export class MainComponentComponent {

  constructor() { }

  // source variable holding the names to display
  NamesToDisplay = [];

  // method for adding a name/card to NamesToDisplay variable
  AddCardToDataDisplay(name:string){
    this.NamesToDisplay.push(name);
  }
}
