import { Component, OnInit,Output,EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.css']
})
export class ToolBarComponent {

  constructor() { }
  //variable for input box
  currentInputContent:string;

  //output event for parent method
  @Output() addCardEvent = new EventEmitter<string>();

  //calls event method
  addCardToParent(value: string): void {
    this.addCardEvent.next(value);
  }

}
