import { Component, OnInit,Input } from '@angular/core';
import { NameApiService } from 'src/app/DataServices/name-api.service';
import { Country } from '../Models/country';

// enum for graph selector 
enum GraphType {
  Bar="Bar",
  Pie="Pie",
}

@Component({
  selector: 'app-dragable-card',
  templateUrl: './dragable-card.component.html',
  styleUrls: ['./dragable-card.component.css']
})
export class DragableCardComponent implements OnInit {
  
  // input for name
  @Input() Name: string; 

  // Graph enum Reference
  allGraphType = GraphType;

  // enumeration of keys for selector box
  graphEnumKeys=[];

  // graph type enum for selection
  graphType:GraphType;

  // Dragable card dimeensions
  width= 600;
  height= 400;

  // data loading variable, if true then graphs are fit to be loaded
  loaded= false;

  // variable for data request result
  RespData:Country[];

  constructor(private nameApiService: NameApiService) {
    this.graphEnumKeys=Object.keys(this.allGraphType);
  }

  ngOnInit(): void {
    this.graphType = GraphType.Bar;
    this.setGraphDataFromName();
  }
     
  setGraphDataFromName(): void {
      this.nameApiService.getCountryProbabilityByName(this.Name).subscribe((res)=>{
        this.RespData= res.country;
        this.loaded = true;
      });      
  
  }
    
  
}
