import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DragableCardComponent } from './dragable-card.component';

describe('DragableCardComponent', () => {
  let component: DragableCardComponent;
  let fixture: ComponentFixture<DragableCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DragableCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DragableCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
