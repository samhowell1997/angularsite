import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Name } from 'src/app/Models/name';
@Injectable({
  providedIn: 'root'
})
export class NameApiService {
  apiURL: string = 'https://api.nationalize.io/';

  constructor(private httpClient: HttpClient) {}

  // method for retreiving probability for each name requested, by country id
  public getCountryProbabilityByName(name:string){
    return this.httpClient.get<Name>(`${this.apiURL}/?name=${name}`);
  }


}
